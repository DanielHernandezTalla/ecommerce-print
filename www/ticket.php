<?php
header("Access-Control-Allow-Origin: *");

require __DIR__ . '/ticket/autoload.php'; //Nota: si renombraste la carpeta a algo diferente de "ticket" cambia el nombre en esta línea

use Mike42\Escpos\EscposImage;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\Printer;

$pedidosEnviados = json_decode(file_get_contents('php://input'), true);

$pathLogo = __DIR__ . '/printLogoKowi.png';
$logoKowi = EscposImage::load($pathLogo);

$nombreImpresora = "BIXOLONErzu";
$connector = new WindowsPrintConnector($nombreImpresora);
$impresora = new Printer($connector);
$impresora->setJustification(Printer::JUSTIFY_CENTER);
$impresora->bitImage($logoKowi);
$impresora->feed(1);
$impresora->text("ALIMENTOS KOWI SA DE CV\n");
$impresora->text("AKO971007558\n");
$impresora->text("CARRETERA FEDERAL MEXICO-NOGALES KM 1788\n");
$impresora->text("NAVOJOA, SONORA C.P. 85230\n");
$impresora->text("\n");
$impresora->setJustification(Printer::JUSTIFY_LEFT);
$impresora->text("FECHA: " . date('d/m/Y H:i:s', strtotime($pedidosEnviados[0]['Fecha_Pago'])) . "\n");
$impresora->text("TICKET: " . $pedidosEnviados[0]['IdDatVentas'] . "\n");
$impresora->text("\n");
//Productos
$impresora->setJustification(Printer::JUSTIFY_LEFT);
$impresora->text("ARTICULO         CANT    PRECIO  IMPORTE\n");
$impresora->text("-----------------------------------------\n");
$impresora->setJustification(Printer::JUSTIFY_LEFT);
foreach ($pedidosEnviados[0]['detalles_ventas'] as $index => $item) {
    $impresora->text(str_pad(substr($item['DesCorta'], 0, 16), 16) . " " . str_pad(number_format($item['CantidadSecundario'], 2), 7) . " " . str_pad(number_format($item['Precio'], 2), 7) . " " . number_format($item['Importe'], 2) . "\n");
}
$impresora->setJustification(Printer::JUSTIFY_RIGHT);
$impresora->text("\n");
$impresora->text("Costo envio: $" . str_pad(substr($pedidosEnviados[0]['Costo_Envio'], 0, 16), 8) . "\n");
$impresora->text("TOTAL: $" . str_pad(substr($pedidosEnviados[0]['Importe_Pago'], 0, 16), 8) . "\n");
$impresora->setJustification(Printer::JUSTIFY_LEFT);
$impresora->text("-----------------------------------------\n");
$impresora->text("\n");
//Fin productos
//Datos del Cliente
$impresora->setJustification(Printer::JUSTIFY_LEFT);
$impresora->text("-------------DATOS DEL CLIENTE-----------\n");
$impresora->text('NOMBRE: ' . $pedidosEnviados[0]['cliente']['Nombre'] . "\n");
$impresora->text('DIRECCION: ' . $pedidosEnviados[0]['cliente']['Direccion'] . "\n");
$impresora->text('COLONIA: ' . $pedidosEnviados[0]['cliente']['Colonia'] . "\n");
$impresora->text('TELEFONO: ' . $pedidosEnviados[0]['cliente']['Telefono'] . "\n");
$impresora->text('CIUDAD: ' . $pedidosEnviados[0]['cliente']['Ciudad'] . "\n");
$impresora->text('CORREO: ' . $pedidosEnviados[0]['cliente']['Email'] . "\n");
$impresora->text("\n");
//Fin Datos del Cliente
$impresora->setJustification(Printer::JUSTIFY_CENTER);
$impresora->text("¡ALTA CALIDAD EN CARNE DE CERDO!\n");
$impresora->text("WWW.KOWI.COM.MX\n");
$impresora->text("¡ GRACIAS POR SU COMPRA=) !\n");
$impresora->feed(2);
$impresora->cut();
$impresora->pulse();
$impresora->close();

http_response_code(201);
echo json_encode(array("message" => "impresion correcta."));
